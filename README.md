# MD WordsCloud

 - Insprite from WordCloud, this module create a block,provide configurations
   for choose vocabulary and show it as MD wordcloud.
   Display of a term with many contents is bigger.

## Table of contents

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers

## Introduction

- This module provides block of the taxonomy terms. The word cloud visually
  represents the terms, with the size of each term adjusted based on the number
  of contents associated with it.


## Requirements

1. D3 library:

 - [Download D3 library from] (https://github.com/d3/d3/releases/tag/v3.0.2)
 - Extract and rename of the releases to `d3`, so the assets are at:
   + **/libraries/d3/d3.min.js**

2. D3 Cloud library:

 - [Download D3 Cloud library from] (https://github.com/jasondavies/d3-cloud)
 - Extract and rename of the releases to `d3-cloud`, so the assets are at:
   + **/libraries/d3-cloud/build/d3.layout.cloud.js**


## Installation

 - Install the Md Wordscloud module as you would normally install a contributed
   Drupal module. 
   [Visit] (https://www.drupal.org/node/1897420) for further information.


## Configuration

 1. Navigate to Administration > Extend and enable the module.
 1. Go to Administation > Structure > Block layout > select a Place block
    button to add a 'MD Words cloud' block to a region.
 1. Use the "Configure" link of the block to configure it accordingly to your
    needs.

## Maintainers

- Rajan Kumar - [rajan-kumar](https://www.drupal.org/u/rajan-kumar)
