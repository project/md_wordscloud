<?php

namespace Drupal\md_wordscloud;

use Drupal\Core\Database\Connection;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Service that handles wordcloud data.
 */
class WordcloudsContent {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The cache store.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheStore;

  /**
   * Database Connection Service.
   *
   * @var Drupal\Core\Database\Connection
   */
  protected $dbConn;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_store
   *   The cache store.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(LanguageManagerInterface $language_manager, CacheBackendInterface $cache_store, Connection $connection) {
    $this->languageManager = $language_manager;
    $this->cacheStore = $cache_store;
    $this->dbConn = $connection;
  }

  /**
   * Get keyword list.
   *
   * @return array
   *   keyword List.
   */
  public function getCloudKeywordList($vids, $size) {
    $cloud_list = $this->getmdcloudTags($vids, $size);
    $terms_url = $words = $counts = [];
    foreach ($cloud_list as $row) {
      $counts[] = $row->count;
      $words[] = $row->name;
      $terms_url[$row->name] = '#';
    }
    $result = (object) [
      'terms_url' => $terms_url,
      'words' => $words,
      'counts' => $counts,
    ];

    return $result;
  }

  /**
   * Returns an array with weighted tags.
   *
   * @param array $vids
   *   A list of array with vocabulary list.
   * @param int $size
   *   The number of words to display.
   *
   * @return array
   *   An array with tags-objects.
   */
  private function getmdcloudTags(array $vids, $size) {
    $tags = [];
    $language = $this->languageManager->getCurrentLanguage();
    $cache_name = implode(':', [
      'mdwordscloud',
      implode('_', $vids),
      $language->getId(),
      $size,
    ]);
    // Check if the cache exists.
    $cache = $this->cacheStore->get($cache_name);
    // Make sure cache has data.
    if (!empty($cache->data)) {
      $tags = $cache->data;
    }
    else {
      $query = $this->dbConn->select('taxonomy_term_data', 'td');
      $query->addExpression('COUNT(td.tid)', 'count');
      $query->fields('tfd', ['name', 'description__value']);
      $query->fields('td', ['tid', 'vid']);
      $query->addExpression('MIN(tn.nid)', 'nid');

      $query->join('taxonomy_index', 'tn', 'td.tid = tn.tid');
      $query->join('node_field_data', 'n', 'tn.nid = n.nid');
      $query->join('taxonomy_term_field_data', 'tfd', 'tfd.tid = tn.tid');

      $query->condition('n.langcode', $language->getId());

      $query->condition('td.vid', $vids, 'IN');
      $query->condition('n.status', 1);
      $query->condition('tfd.status', 1);

      $query->groupBy('td.tid')->groupBy('td.vid')->groupBy('tfd.name');
      $query->groupBy('tfd.description__value');

      $query->having('COUNT(td.tid)>0');
      $query->orderBy('count', 'DESC');

      if ($size > 0) {
        $query->range(0, $size);
      }
      $result = $query->execute()->fetchAll();

      foreach ($result as $tag) {
        $tags[$tag->tid] = $tag;
      }

      $this->cacheStore->set($cache_name, $tags, CacheBackendInterface::CACHE_PERMANENT, [
        'node_list',
        'taxonomy_term_list',
      ]);
    }

    return $tags;
  }

}
