<?php

namespace Drupal\md_wordscloud\Plugin\Block;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\md_wordscloud\WordcloudsContent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Md Words cloud' block.
 *
 * @Block(
 *   id = "mdwordscloud",
 *   admin_label = @Translation("MD Words cloud")
 * )
 */
class MdWordscloud extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The words cloud service.
   *
   * @var \Drupal\md_wordscloud\WordcloudsContent
   */
  public $wordCloudsService;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\md_wordscloud\WordcloudsContent $wordCloudsService
   *   wordCloudsService description.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, WordcloudsContent $wordCloudsService, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->Wordcloudsservice = $wordCloudsService;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('md_wordscloud.wordclouds'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'cache' => [
        'max_age' => 0,
        'contexts' => [],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $terms_limit = $this->configuration['max_words'] ?? 100;
    $vocabulary_list = $this->configuration['vocabulary'] ?? [];
    $def_vocabulary = [];
    if (!empty($this->configuration['vocabulary'])) {
      $def_vocabulary = array_filter($vocabulary_list);
    }

    $vocabularies = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
    $vocabulary_options = [];
    foreach ($vocabularies as $vocabulary) {
      $vid = $vocabulary->get('vid');
      $name = $vocabulary->get('name');
      $vocabulary_options[$vid] = $name;
    }
    $form['vocabulary'] = [
      '#type' => 'checkboxes',
      '#options' => $vocabulary_options,
      '#title' => $this->t('Vocabularies'),
      '#required' => TRUE,
      '#default_value' => $def_vocabulary,
    ];

    $form['max_words'] = [
      '#type' => 'number',
      '#title' => $this->t('Max words to display'),
      '#default_value' => $terms_limit,
      '#maxlength' => 3,
      '#attributes' => ['min' => 0, 'max' => 100],
      '#required' => TRUE,
      '#description' => $this->t("The number of tags to show in this block. Enter '0' to display all tags."),
    ];

    $form['width_height'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'md-angle',
        ],
      ],
    ];
    $form['width_height']['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width:'),
      '#required' => TRUE,
      '#attributes' => ['min' => 100, 'max' => 1920],
      '#default_value' => (!empty($this->configuration['width'])) ? $this->configuration['width'] : 300,
      '#field_suffix' => 'px',
    ];

    $form['width_height']['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height:'),
      '#required' => TRUE,
      '#attributes' => ['min' => 100, 'max' => 1000],
      '#default_value' => (!empty($this->configuration['height'])) ? $this->configuration['height'] : 300,
      '#field_suffix' => 'px',
    ];

    $form['words_scale'] = [
      '#type' => 'radios',
      '#title' => $this->t('Words scale'),
      '#required' => TRUE,
      '#options' => [
        'log' => $this->t('log n'),
        'sqrt' => $this->t('√n'),
        'linear' => $this->t('n'),
      ],
      '#default_value' => (!empty($this->configuration['words_scale'])) ? $this->configuration['words_scale'] : 'linear',
    ];

    $form['angle'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'md-angle',
        ],
      ],
    ];

    $form['angle']['angle_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Words orientation'),
      '#required' => TRUE,
      '#default_value' => (!empty($this->configuration['angle_count'])) ? $this->configuration['angle_count'] : 5,
      '#attributes' => ['min' => 0, 'max' => 100],
    ];

    $form['angle']['angle_from'] = [
      '#type' => 'number',
      '#title' => $this->t('From:'),
      '#required' => TRUE,
      '#default_value' => (!empty($this->configuration['angle_from'])) ? $this->configuration['angle_from'] : -90,
      '#attributes' => ['min' => -90, 'max' => 90],
      '#field_suffix' => '&deg; to ',
    ];

    $form['angle']['angle_to'] = [
      '#type' => 'number',
      '#title' => $this->t('to:'),
      '#required' => TRUE,
      '#attributes' => ['min' => -90, 'max' => 90],
      '#default_value' => (!empty($this->configuration['angle_to'])) ? $this->configuration['angle_to'] : 90,
      '#field_suffix' => '&deg;',
    ];

    $form['#attached']['library'][] = 'md_wordscloud/md_wordscloud_style';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['max_words'] = $form_state->getValue('max_words');
    $this->configuration['vocabulary'] = $form_state->getValue('vocabulary');
    $this->configuration['width'] = $form_state->getValue('width_height')['width'];
    $this->configuration['height'] = $form_state->getValue('width_height')['height'];
    $this->configuration['words_scale'] = $form_state->getValue('words_scale');
    $this->configuration['angle_count'] = $form_state->getValue('angle')['angle_count'];
    $this->configuration['angle_from'] = $form_state->getValue('angle')['angle_from'];
    $this->configuration['angle_to'] = $form_state->getValue('angle')['angle_to'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $vids = $this->configuration['vocabulary'];
    $max_words = $this->configuration['max_words'] ?? 100;
    $width = $this->configuration['width'];
    $height = $this->configuration['height'];
    $angle_from = $this->configuration['angle_from'];
    $angle_to = $this->configuration['angle_to'];
    $angle_count = $this->configuration['angle_count'];
    $words_scale = $this->configuration['words_scale'];

    $result = $this->Wordcloudsservice->getCloudKeywordList($vids, $max_words);

    $blocks_data = [
      'block_id' => 1,
      'width' => $width,
      'height' => $height,
      'angle_from' => $angle_from,
      'angle_to' => $angle_to,
      'angle_count' => $angle_count,
      'words_scale' => $words_scale,
      'terms_url' => $result->terms_url,
      'words' => $result->words,
      'counts' => $result->counts,
    ];

    $build = [
      '#theme' => 'blockmdwordcloud',
      '#attached' => [
        'library' => [
          'md_wordscloud/md_wordscloud_scripts',
          'md_wordscloud/md_wordscloud.d3',
          'md_wordscloud/md_wordscloud.d3cloud',
        ],
        'drupalSettings' => [
          'md_wordscloud' => [
            'blocks_data' => [$blocks_data],
          ],
        ],
      ],
      '#content' => [],
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), [
      'node_list',
      'taxonomy_term_list',
    ]);
  }

}
